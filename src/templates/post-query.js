import { graphql } from 'gatsby';
import PostPage from '../components/post';

export default PostPage;

export const query = graphql`
  query PostPageQuery($id: String!) {
    mdx(id: { eq: $id }) {
      body
    }
  }
`;
