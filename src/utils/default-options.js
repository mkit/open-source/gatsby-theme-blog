module.exports = themeOptions => {
  // name for the content root folder
  const contentPath = themeOptions.contentPath || 'content';

  // mapping between MDX and relations like authors, tags, etc.; {} to disable
  const mapping = themeOptions.mapping || {
    'Mdx.frontmatter.author': 'AuthorsYaml'
  };

  return {
    contentPath,
    mapping
  };
};
