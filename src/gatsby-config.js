const remarkUnwrapImagesPlugin = require('remark-unwrap-images');

const withDefaults = require('./utils/default-options');

module.exports = themeOptions => {
  const options = withDefaults(themeOptions);

  return {
    mapping: {
      ...options.mapping
    },
    plugins: [
      {
        resolve: 'gatsby-source-filesystem',
        options: {
          path: options.contentPath
        }
      },
      'gatsby-transformer-yaml',
      'gatsby-plugin-sharp',
      {
        resolve: 'gatsby-plugin-mdx',
        options: {
          plugins: ['gatsby-remark-images'], // workaround because of https://github.com/gatsbyjs/gatsby/issues/15486#issuecomment-510153237
          remarkPlugins: [remarkUnwrapImagesPlugin], // see https://github.com/remarkjs/remark-unwrap-images
          gatsbyRemarkPlugins: [
            {
              resolve: 'gatsby-remark-images',
              options: {
                maxWidth: 1920,
                wrapperStyle:
                  'width: 100%; margin-left: 0; margin-right: 0; text-align: center; font-style: italic;',
                linkImagesToOriginal: false,
                showCaptions: true
              }
            }
          ]
        }
      }
    ]
  };
};
