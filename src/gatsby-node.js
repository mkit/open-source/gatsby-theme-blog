const path = require('path');
const fs = require('fs');
const ncp = require('ncp');
const readingTime = require('reading-time');
const { createFilePath } = require('gatsby-source-filesystem');
const withDefaults = require('./utils/default-options');

const PostTemplate = require.resolve(`./templates/post-query`);

/**
 * Ensure the content filesystem required by the blog is present.
 *
 * If not, copy an example filesystem to the plugin's consumer site.
 */
exports.onPreBootstrap = async (
  { store, reporter },
  themeOptions,
  callback
) => {
  const { contentPath } = withDefaults(themeOptions);

  // source
  const pluginDir = __dirname;
  const pluginExampleContentPath = path.resolve(pluginDir, 'content');

  // destination
  const { program } = store.getState();
  const siteDir = program.directory;
  const siteContentPath = path.resolve(siteDir, contentPath);

  const hasContentFs = fs.existsSync(siteContentPath);
  if (hasContentFs) {
    callback();
  } else {
    reporter.warn(
      `creating the required blog filesystem at "${siteContentPath}"`
    );

    ncp(pluginExampleContentPath, siteContentPath, err => {
      if (err) {
        reporter.panic(err);
      }
      callback();
    });
  }
};

/**
 * Add `slug` and `readingTime` to all MDX Node types.
 *
 * `slug` gets auto generated based on the folder name of the post under `content/blog/`.
 * `readingTime` gets auto generated based on the MDX's raw body length and `reading-time` lib.
 */
exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions;

  if (node.internal.type === 'Mdx') {
    createNodeField({
      name: 'slug',
      node,
      value: createFilePath({
        node,
        getNode,
        basePath: 'posts',
        trailingSlash: false
      })
    });

    createNodeField({
      name: 'readingTime',
      node,
      value: readingTime(node.rawBody)
    });
  }
};

/**
 * Create all blog post pages.
 */
exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions;
  const { data, errors } = await graphql(`
    {
      allMdx {
        edges {
          node {
            id
            fields {
              slug
            }
          }
        }
      }
    }
  `);

  if (errors) {
    reporter.panicOnBuild(errors);
    return;
  }

  // Create all individual blog post pages
  data.allMdx.edges.forEach(({ node }) => {
    createPage({
      path: node.fields.slug,
      component: PostTemplate,
      context: { id: node.id }
    });
  });
};
